﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ng_Rusia2018.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ng_Rusia2018.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var listaPaises = new[]
            {
                new Banderas {CodigoPais ="RUS",NombrePais="Rusia", Left=0,Top=0,ListaPartidos=new[]
                    {
                        new Partidos {Partido=1, FechaHora="14/06",Estadio="Moscu",Equipo1="RUS",Equipo2="DEU"},
                        new Partidos {Partido=17, FechaHora="18/06", Estadio="San Petersburgo", Equipo1="RUS",Equipo2="CMR" },
                        new Partidos {Partido=33, FechaHora="22/06", Estadio="Kaliningrado", Equipo1="HUN",Equipo2="RUS" }
                    }
                },

                new Banderas { CodigoPais ="DEU",NombrePais="Alemania",Left=125,Top=0,ListaPartidos=new[]
                    {
                        new Partidos {Partido=1, FechaHora="14/06",Estadio="Moscu",Equipo1="RUS",Equipo2="DEU"},
                        new Partidos {Partido=18, FechaHora="19/06", Estadio="Kaliningrado", Equipo1="DEU",Equipo2="HUN" },
                        new Partidos {Partido=34, FechaHora="23/06", Estadio="Volgogrado", Equipo1="CMR",Equipo2="DEU" }
                    }
                },

                new Banderas { CodigoPais ="CMR",NombrePais="Camerun",Left=250,Top=0,ListaPartidos=new[]
                    {
                        new Partidos {Partido=2, FechaHora="15/06", Estadio="Moscu", Equipo1="RUS",Equipo2="CMR" },
                        new Partidos {Partido=17, FechaHora="18/06", Estadio="San Petersburgo", Equipo1="CMR",Equipo2="HUN" },
                        new Partidos {Partido=34, FechaHora="17/06", Estadio="Volgogrado", Equipo1="CMR",Equipo2="DEU" }
                    }
                }
            };

            var setting = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(),
                TypeNameHandling =TypeNameHandling.Objects
            };
            var serializerDataListaPaises = JsonConvert.SerializeObject(listaPaises, Formatting.Indented, setting);

            var data = new Data
            {
                ListaPaises = serializerDataListaPaises
            };

            return View(data);
        }
    }
}