﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ng_Rusia2018.Models
{
    public class Partidos
    {
        public int Partido { get; set; }

        public string FechaHora { get; set; }

        public string Estadio { get; set; }

        public string Equipo1 { get; set; }

        public string ResultadoEquipo1 { get; set; }

        public string Equipo2 { get; set; }

        public string ResultadoEquipo2 { get; set; }
    }
}