﻿var RegistrationModule = angular.module("RegistrationModule", ["ngRoute"])
    .config(function ($routeProvider, $locationProvider) {

        $routeProvider.when('/', {
            templateUrl: '/App/Templates/bienvenidos.html',
            controller: 'bienvenidosController'
        });

        $routeProvider.when('/fasedegrupos', {
            templateUrl: '/App/Templates/fasedegrupos.html',
            controller: 'fasedegruposController'
        });

        $routeProvider.when('/octavos', {
            templateUrl: '/App/Templates/octavos.html',
            controller: 'octavosController'
        });

        $routeProvider.when('/cuartos', {
            templateUrl: '/App/Templates/cuartos.html',
            controller: 'cuartosController'
        });

        $routeProvider.when('/semifinalesfinal', {
            templateUrl: '/App/Templates/semifinalesfinal.html',
            controller: 'semifinalesfinalController'
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    })
    .directive('myRepeatDirective', function ($timeout) {
        return function (scope, element, attr) {
            var randomTime = Math.floor((Math.random() * 11) + 1) * 100;
            element.css('top', attr.nuevaposiciontop + "px");
            $timeout(function () {
                element.css('left', attr.nuevaposicionleft + "px");
            }, randomTime)
        };
    });